/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.com;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Luiz_
 */

public class Servidor {

    public static String getMyIpAddress(){
        
        String ipAddress = "";
        
        try{
            final InetAddress address = InetAddress.getLocalHost();
            ipAddress = address.getHostAddress();
        }catch( UnknownHostException ex ){
            System.err.println("Erro ao pegar host [WHAT] " + ex.getMessage() );
        }
        
        return ipAddress;
        
    }

    public static void main(String[] args) {

         try {

            final int nrPorta = 5555;
            final ServerSocket serverSocket = new ServerSocket(nrPorta);
            System.out.println(String.format( "Servidor aguardando conexões [IP] %s [PORTA] %d", getMyIpAddress(), nrPorta ) );

            final Socket clientSocket = serverSocket.accept();
            System.out.println("Cliente conectado [IP_CLIENTE] " + clientSocket.getInetAddress().getHostAddress());

            while ( true ) {
                
                byte[] buffer = new byte[2048];
                final int bytesRead = clientSocket.getInputStream().read(buffer);
                final String mensagem = new String(buffer, 0, bytesRead).trim();

                if ( mensagem.isEmpty() ) {
                    break;
                }

                System.out.println("Mensagem recebida do cliente: " + mensagem);
                
            }

            clientSocket.close();
            serverSocket.close();

        } catch ( IOException ex ) {
            System.err.println( "Erro de entrada ou saida [WHAT] " + ex.getMessage() );
        } catch ( Exception ex ){
            System.err.println( "Erro generico [WHAT] " + ex.getMessage() );
        }
    }
}
