/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.com;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Luiz_
 */
public class Cliente {
    
 public static void main(String[] args) {
        try {
            
            final Scanner inputScanner = new Scanner( System.in );
           
            System.out.print("Informe o IP: ");
            final String dsIpAddress = inputScanner.nextLine();
            
            System.out.print("Informe a PORTA: ");
            final int nrPorta = Integer.parseInt( inputScanner.nextLine() );
            
            System.out.println( String.format( "[IP] %s [PORTA] %d", dsIpAddress, nrPorta ) );
   
            final Socket clienteSocket = new Socket(dsIpAddress, nrPorta );
            System.out.println("Conectado ao servidor.");
            
            final PrintStream outputStream = new PrintStream( clienteSocket.getOutputStream() );
            
            inputScanner.reset();
            
            String currentMessage;
            
            while( inputScanner.hasNextLine() ){
                
                currentMessage = inputScanner.nextLine();
                outputStream.println( currentMessage );
                
                if( currentMessage.isEmpty() ){
                    break;
                }
                
            }
            
            outputStream.close();
            inputScanner.close();
            clienteSocket.close();
            
        } catch ( IOException ex ) {
            System.err.println( "Erro de entrada ou saida [WHAT] " + ex.getMessage() );
        } catch ( NumberFormatException ex ){
            System.err.println( "Erro ao converter [WHAT] " + ex.getMessage() );
        } catch ( Exception ex ){
            System.err.println( "Erro generico [WHAT] " + ex.getMessage() );
        }
    }
}
