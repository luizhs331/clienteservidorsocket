import socket

def main():
    # Define o endereço IP e a porta do servidor
    server_ip = "127.0.0.1"  # Endereço IP do servidor
    server_port = 5555  # Porta do servidor

    # Cria o socket do cliente TCP
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Conecta-se ao servidor TCP
        client_socket.connect((server_ip, server_port))
        print("Conectado ao servidor.")

        while True:
            # Aguarda a entrada do usuário
            mensagem = input("Digite a mensagem a ser enviada (ou 'sair' para encerrar): ")

            if mensagem.lower() == "sair":
                break

            # Envia a mensagem para o servidor TCP
            client_socket.send(mensagem.encode())

    except ConnectionRefusedError:
        print("Não foi possível conectar ao servidor.")
    except KeyboardInterrupt:
        print("Encerrando o cliente.")

    # Fecha o socket do cliente TCP
    client_socket.close()

if __name__ == "__main__":
    main()