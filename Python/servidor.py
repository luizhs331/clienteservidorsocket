import socket

def main():
    # Define o endereço IP e a porta do servidor
    server_ip = "127.0.0.1"  # Endereço IP do servidor
    server_port = 5555  # Porta do servidor

    # Cria o socket do servidor
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Vincula o socket do servidor ao endereço IP e porta
        server_socket.bind((server_ip, server_port))
        print("Servidor iniciado. Aguardando conexões...")

        # Aguarda até 5 conexões simultâneas
        server_socket.listen(5)

        while True:
            # Aceita uma nova conexão do cliente
            client_socket, client_address = server_socket.accept()
            print("Cliente conectado:", client_address)

            while True:
                # Recebe a mensagem do cliente
                mensagem = client_socket.recv(1024).decode()

                if not mensagem:
                    break

                # Processa a mensagem recebida
                print("Mensagem recebida do cliente:", mensagem)

            # Encerra a conexão com o cliente
            client_socket.close()

    except KeyboardInterrupt:
        print("Encerrando o servidor.")

    # Fecha o socket do servidor
    server_socket.close()

if __name__ == "__main__":
    main()